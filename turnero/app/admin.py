from django.contrib import admin
from . import models

# Register your models here.


class ClienteAdmin(admin.ModelAdmin):
    list_display= ["cliente_id","nombre", "apellido"]


class PrioridadAdmin(admin.ModelAdmin):
    list_display= ["prioridad_id","prioridad_descripcion", "nivel"]

class EstadoAdmin(admin.ModelAdmin):
    list_display= ["estado_id","estado_descripcion"]

class ServicioAdmin(admin.ModelAdmin):
    list_display= ["servicio_id","servicio_descripcion"]



admin.site.register(models.Cliente)
admin.site.register(models.Prioridad, PrioridadAdmin)
admin.site.register(models.Estado, EstadoAdmin)
admin.site.register(models.Servicio, ServicioAdmin)
admin.site.register(models.Turno_ticket)
admin.site.register(models.Cola_espera)
