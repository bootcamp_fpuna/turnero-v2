from datetime import datetime
import os
from django.utils import timezone
from django.shortcuts import render, redirect
from . import forms
from django.contrib import messages
from . import models
from django.db.models import Q
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib import admin
import heapdict, operator

# Create your views here.


#funcion para pagina de inicio
def inicio(request):
    context={
    }
    return render(request, 'base.html', context)



#funcion para cargar la cola
def cargar_cola(cola_servicio):
    #se crea la cola de prioridad
    cola2 = heapdict.heapdict()
    cola= cola_servicio.filter(estado__estado_descripcion = "En Espera")
    for item in cola:
    #se carga de acuerdo a la prioridad
        if item.prioridad.nivel=='Alta':
            # print('alta')
            #se ingresa a la cola con la maxima prioridad   
            cola2[item.numero_turno]=1
        elif item.prioridad.nivel=='Media':
            # print('media')
            cola2[item.numero_turno]=2
        elif item.prioridad.nivel=='Baja':
            #se ingresa a la cola con la menor prioridad
            # print('baja')
            cola2[item.numero_turno]=3  
    #cola ordenada      
    cola_sort = sorted(cola2.items(), key=operator.itemgetter(1))
    for name in enumerate(cola_sort):
        print(name[1][0], cola2[name[1][0]])
    return cola_sort




#funcion para mostrar el formulario de carga de datos de las personas en un html
def carga_datos_cliente(request):
    form= forms.CargaDatosClienteForm(request.POST or None)
    if request.method=="POST":
        if form.is_valid:     
            form.save()
            return redirect('inicio')
        else:
            form= forms.CargaDatosClienteForm()
            messages.error("No se guardaron los datos")
    context= {
        'form': form,
    }
    return render(request,'formulario_clientes.html',context)



def buscar_cliente(request):
    clientes= []
    #con el formualrio get se busca al cliente
    if request.method=="GET":
        solicitud_busqueda = request.GET.get("buscar")
        #busqueda del cliente
        if solicitud_busqueda:
            #busca por ruc y nro de documento
            clientes = models.Cliente.objects.filter(
                Q(nro_documento__icontains = solicitud_busqueda) )
    #con el formulario post se captura a la persona seleccionada y se envia a la siguiente funcion
    elif request.method=="POST":
        cliente_seleccionado= request.POST.get('cliente_seleccionado',False)
        if cliente_seleccionado != False:
            pk_cliente = models.Cliente.objects.get(nro_documento=cliente_seleccionado)
            print(pk_cliente.cliente_id)
            return redirect('otorgar_turno',pk=pk_cliente.cliente_id)
        else: 
            messages.error(request,"No se selecciono a un cliente")
    context= {
        'cliente': clientes,
    }
    return render(request,'buscar_cliente.html',context)



# #funcion para crear turno, la pk es el id del cliente
# def crear_turno(request,pk):
#     #Se busca al cliente seleccionado por su id
#     cliente= models.Cliente.objects.get(cliente_id= pk)
#     #datos del turno
#     espera= models.Estado.objects.get(estado_descripcion ="En Espera")
#     numero= "0"+str(models.Turno_ticket.objects.count() + 1)# temporal se puede mejorar
#     fecha= datetime.now().date()
#     hora= datetime.now().time()
#     #se carga en un diccionario
#     dato_form = {
#         'cliente': cliente,
#         'estado': espera,
#         'numero_turno': numero,
#         'fecha_ticket': fecha,
#         'hora_ticket': hora,
#     }
#     #se carga en el formulario de turno a mostrar en pantalla
#     form= forms.TurnoForm(data= dato_form)

#     if request.method=="POST" and "BotonGuardar" in request.POST: 
#         form= forms.TurnoForm(request.POST or None)
#         if form.is_valid: 
#             print("prueba")    
#             #instance=form.save()
#             print(form.cliente)
#             return redirect('inicio')
#     context= {
#         'form': form,
#     }
#     return render(request,'otorgar_turno.html',context)



def crear_turno(request,pk):
    cola_aux = models.Cola_espera.objects.get(servicio__servicio_descripcion="---")
    prioridad_aux = models.Prioridad.objects.get(prioridad_descripcion="---")
    #Se busca al cliente seleccionado por su id
    cliente= models.Cliente.objects.get(cliente_id= pk)
    #datos del turno
    espera= models.Estado.objects.get(estado_descripcion ="En Espera")
    # numero= "0"+str(models.Turno_ticket.objects.count() + 1)# temporal se puede mejorar
    fecha= datetime.now().date()
    hora= datetime.now().time()
    inicial= models.Turno_ticket.objects.create(cliente= cliente,estado= espera,
        # numero_turno= numero,
        fecha_ticket= fecha,
        hora_ticket=hora,
        cola=cola_aux,
        prioridad=prioridad_aux
        )
    form= forms.TurnoForm(data=request.POST or None, instance=inicial)
    inicial.delete() 
    if request.method=="POST" and "BotonGuardar" in request.POST: 
        if form.is_valid:
            print("prueba") 
            instance = form.save(commit= False)
            if instance.cola != cola_aux and instance.prioridad != prioridad_aux:
                cadena= instance.cola.servicio.servicio_descripcion
                instance.numero_turno= cadena[:2].upper() +str(models.Turno_ticket.objects.filter(
                            cola__servicio__servicio_descripcion=cadena).count() + 1) 
                instance.save()
                return redirect('inicio')
            else: 
               messages.error(request,"No se seleccionó la cola o la prioridad")   
    context= {
        'form': form,
    }
    return render(request,'otorgar_turno.html',context)






def ver_cola(request,servicio):
    #turnos atendidos
    #cola_atendidos_servicio me trae todos los objetos de la tabla Turno_ticket 
    # que coincidan con el servicio solicitado
    cola_servicio=models.Turno_ticket.objects.filter(
        cola__servicio__servicio_descripcion=servicio).filter(
        fecha_ticket= datetime.now().date())

    #cola_atendidos me trae todos los objetos que estan en la variable cola_atendidos_servicio pero que
    #ademas coincidan con la fecha de hoy y el estado Atendido
    cola_atendidos= cola_servicio.exclude(estado__estado_descripcion=
                    "En Turno").exclude(estado__estado_descripcion="Cancelado").exclude(
                    estado__estado_descripcion="En Espera")
    
    #en turno me trae todos los objetos que estan en la variable cola_atendidos_servicio pero que
    #ademas coincidan con la fecha de hoy y el estado En Turno
    en_turno= cola_servicio.filter(estado__estado_descripcion=
                    "En Turno")
    #llama a funcion que carga la cola de los turnos en espera
    cola_sort= cargar_cola(cola_servicio)
    context= {
         'cola': cola_sort[:10] ,
         'cola_atendidos':cola_atendidos[:10],
         'en_turno':en_turno[:10],
    }
    return render(request,'ver_cola.html',context)



def atender(request,servicio,n):
     
    #se seleccionan los turnos del dia y que correspondan al servicio indicado
    cola_servicio= models.Turno_ticket.objects.filter(
        cola__servicio__servicio_descripcion=servicio,
        fecha_ticket= datetime.now().date()
    )

    #se carga la cola con los turnos que tengan estado En Espera
    cola=cargar_cola(cola_servicio)

    #se busca si hay un turno con estado En Turno
    turno_llamado=cola_servicio.filter(estado=models.Estado.objects.get(estado_descripcion="En Turno"))

    #si la cola esta vacia se tira un mensaje
    if len(cola)<1:
        turno_siguiente="---"
    else:
        #se carga el turno siguiente con el primer elemento de la cola
        turno_siguiente=cola[0][0]
        print((turno_llamado))
        
        #si se presiono el boton que tiene name=llamado, se llama al numero y se indica que esta
        # en estado En Turno.
        if "llamado" in request.POST:

            #se busca en la tabla el objeto que tenga el primer numero de la cola
            turno_llamado=cola_servicio.filter(numero_turno=turno_siguiente)

            #se actualiza su estado a En Turno
            turno_llamado.update(estado=models.Estado.objects.get(estado_descripcion="En Turno"))

            #se redirige para resetear el valor de los botones y se cambia el valor de la variable de
            #control
            return redirect('atender',servicio=servicio, n=1)
    #si hay un turno con estado En Turno
    if len(turno_llamado) >0:

        #se cambia el valor de la variable de control para visualizar correctamente en el template
        n=1

        #si se presiono el boton que dice abandonado,se indica que no se presento 
        # con el estado Abandonado   
        # si se presiono el boton que dice atendido,se indica que se atendio al cliente 
        if "abandonado" in request.POST:

            #se actualiza su estado a Abandonado
            turno_llamado.update(estado=models.Estado.objects.get(estado_descripcion="Abandonado"))

            #se redirige para resetear el valor de los botones y se cambia el valor de la variable de
            #control
            return redirect('atender',servicio=servicio, n=2)
        elif "atendido" in request.POST:

            #se actualiza su estado a Atendido
            turno_llamado.update(estado=models.Estado.objects.get(estado_descripcion="Atendido"))
            
            #se redirige para resetear el valor de los botones y se cambia el valor de la variable de
            #control
            return redirect('atender',servicio=servicio,n=3)
    #si no hay se carga con vacio
    else:
        turno_llamado=""

    #la variable de control nos permite recuperar el turno atendido o abandonado ya que al hacer el update
    #del estado recarga la funcion o si no se hizo un update nos permite visualizar correctamente el 
    #turno llamado
    if n==1:
            turno_llamado= turno_llamado.get()
    elif n==2:
            turno_llamado=cola_servicio.filter(estado=models.Estado.objects.get(estado_descripcion="Abandonado")).last()      
    elif n==3:
            turno_llamado=cola_servicio.filter(estado=models.Estado.objects.get(estado_descripcion="Atendido")).last()

    context= {
        'turno_siguiente':turno_siguiente,
        'turno_llamado': turno_llamado ,
        'n':n,
    }
    return render(request,'en_atencion.html',context)


#se puede hacer una funcion general, pero dejare para el ultimo si hay tiempo
def filtar_servicios(request,accion):
    servicios= []
    if request.method=="GET":
        solicitud_busqueda = request.GET.get("buscar")
        print(solicitud_busqueda)
        if solicitud_busqueda:
            servicios = models.Servicio.objects.filter(
                Q(servicio_descripcion__icontains = solicitud_busqueda) )
            print(servicios)
    elif request.method=="POST":
        servicio_seleccionado= request.POST.get('servicio_seleccionado',False)
        print(servicio_seleccionado)
        if servicio_seleccionado != False:
            pk_servicio = models.Servicio.objects.get(servicio_descripcion=servicio_seleccionado)
            if accion=='atender':
                return redirect(accion,servicio=pk_servicio, n=0)
            else:
                return redirect(accion,servicio=pk_servicio)
            # return redirect('ver_cola',servicio=pk_servicio)
        else: 
            messages.error(request,"No se selecciono a un servicio")
    context= {
        'servicio': servicios,
    }
    return render(request,'filtrar_servicios.html',context)


def cancelar_o_derivar(request):
    print("inicio")
    form=' '
    turnos= []
    if request.method=="GET":
        print("get")
        solicitud_busqueda = request.GET.get("buscar")
        if solicitud_busqueda:
            turnos = models.Turno_ticket.objects.filter(
                Q(cliente__nro_documento__icontains = solicitud_busqueda) )
            turnos=turnos.filter(fecha_ticket= datetime.now().date())
    elif request.method=="POST":
        print("post")
        turno_seleccionado= request.POST.get('turno_seleccionado',False)
        print(turno_seleccionado)
        if turno_seleccionado != False:
            pk_turno = models.Turno_ticket.objects.get(numero_turno=turno_seleccionado)
            pk_turno= pk_turno.turno_id
            turno_actualizar=models.Turno_ticket.objects.get(turno_id= pk_turno)
            print(turno_actualizar.turno_id)
            form= forms.ActualizarTurnoForm(instance=turno_actualizar)
            # print(form.has_changed())
            print("cargo el formulario")
            # return redirect('cancelar_o_derivar',form=form)     
        else: 
            # form= forms.ActualizarTurnoForm(request.POST,instance=turno_actualizar)
            print("cargo el formulario2")
            form=forms.ActualizarTurnoForm(request.POST or None)
            instance = form.save(commit=False)
            print(instance.numero_turno)
            fecha= datetime.now().date()
            hora= datetime.now().time()
            cadena= instance.cola.servicio.servicio_descripcion
            nuevo_numero_turno= cadena[:2].upper() +str(models.Turno_ticket.objects.filter(
                            cola__servicio__servicio_descripcion=cadena).count() + 1) 
            models.Turno_ticket.objects.filter(numero_turno=instance.numero_turno).update(
                        cola=instance.cola, 
                        estado=instance.estado, 
                        hora_ticket=hora,
                        fecha_ticket= fecha,
                        numero_turno=nuevo_numero_turno)
            return redirect('inicio')
            messages.error(request,"No se selecciono a un turno")

            
    context= {
        'turnos': turnos,
        'form':form,
    }
    return render(request,'cancelar_o_derivar.html',context)



def cerrar(request):
    logout(request)
    return redirect('login')


def login_usuario(request):
    if request.method=='POST':
        usuario=request.POST['username']
        password=request.POST['password']
        user=authenticate(username=usuario,password=password)
        if user is not None:
            print(user.get_username())
            login(request, user)
            usuario=User.objects.get(username=user.get_username())
            if usuario.is_staff==True:
                #para redigir al admin si es administrador
                return redirect('admin:login')
            elif usuario.is_active:
                return redirect('inicio')
        else:
            messages.error (request, "Usuario o contraseña incorrecta")
            return redirect('login')
    return render(request,"login.html",{})

