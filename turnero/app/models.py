
from django.db import models


class Cliente(models.Model):
    cliente_id = models.AutoField(primary_key=True)
    nro_documento = models.CharField(max_length=20)
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    nro_telefono = models.CharField(max_length=15)
    email = models.EmailField(max_length=150) 

    def __str__(self) -> str:
         return str(self.nro_documento)
    
    class Meta:
        db_table = 'cliente'



class Servicio(models.Model):
    servicio_id = models.AutoField(primary_key=True)
    servicio_descripcion = models.CharField(max_length=20)
    estado_servicio = models.BooleanField()
    usuario_insercion = models.CharField(max_length=16, null=True)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=16, null=True)
    fecha_modificacion = models.DateTimeField(null=True)   

    def __str__(self) -> str:
        return self.servicio_descripcion

    class Meta:
        db_table = 'servicio'        



class Prioridad(models.Model):
    prioridad_id = models.AutoField(primary_key=True)
    nivel = models.CharField(max_length=20)
    prioridad_descripcion = models.CharField(max_length=20)
    estado_prioridad = models.BooleanField()
    usuario_insercion = models.CharField(max_length=16, null=True)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=16, null=True)
    fecha_modificacion = models.DateTimeField(null=True)  

    def __str__(self) -> str:
        return self.prioridad_descripcion

    class Meta:
        db_table = 'prioridad'



class Cola_espera(models.Model):
    cola_id = models.AutoField(primary_key=True)
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)
    turno_anterior = models.CharField(max_length=10, blank=True, null=True)
    turno_actual = models.CharField(max_length=10, blank=True, null=True)
    turno_siguiente = models.CharField(max_length=10, blank=True, null=True)
    usuario_insercion = models.CharField(max_length=16, null=True)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=16, null=True)
    fecha_modificacion = models.DateTimeField(null=True) 

    class Meta:
        db_table = 'cola_espera'

    def __str__(self) -> str:
        return self.servicio.servicio_descripcion



class Estado(models.Model):
    estado_id = models.AutoField(primary_key=True)
    estado_descripcion = models.CharField(max_length=20)

    def __str__(self) -> str:
        return self.estado_descripcion
    
    class Meta:
        db_table = 'estado'



class Turno_ticket(models.Model):
    turno_id = models.AutoField(primary_key=True)
    cola = models.ForeignKey(Cola_espera, on_delete=models.CASCADE)
    estado = models.ForeignKey(Estado, on_delete=models.CASCADE)
    prioridad = models.ForeignKey(Prioridad, on_delete=models.CASCADE)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    numero_turno = models.CharField(max_length=5)
    fecha_ticket = models.DateField()
    hora_ticket = models.TimeField()

    def __str__(self) -> str:
        return self.numero_turno
    
    class Meta:
        db_table = 'turno_ticket'        

